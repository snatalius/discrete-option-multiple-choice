var ObjectId = require('mongodb').ObjectID;

/**
 * Gets total number of questions available in the database.
 * @param {*} db    Database object
 */
const getTotalNumQuestion = async (db) => {
    return await db.collection('questions').countDocuments();
}
exports.getTotalNumQuestion = getTotalNumQuestion;

/**
 * Creates new game and stores it in the database.
 * @param {*} db            Database object
 * @param {*} playerName    Player name
 * @param {*} numQuestions  Number of question
 */
const createNewGame = async (db,playerName,numQuestions) => {
    console.log(`Create a new game for player name ${playerName} with ${numQuestions} question(s)`);
    var qSet = await compileQuestions(db,numQuestions);
    const game = {playerName: playerName, score: null, questionSet: qSet, resultSet: null, createdOn: new Date()};
    await db.collection('games').insertOne(game);
    return game;
}
exports.createNewGame = createNewGame;

/**
 * Compiles questions that will be played in a new game.
 * How this works?
 * 1. A list of N random questions is aggregated from the Database.
 * 2. For each question, the function will check its type.
 *    (a) If it's MC, get 2 to N answer options in a random manner. These are options to be played in the game.
 *    (b) If it's TF, set the expected answer the same as what's stored.
 * @param {*} db    Database object
 * @param {*} num   Number of questions to be compiled
 */
const compileQuestions = async (db,num) => {
    var listQuestions = await db.collection('questions').aggregate([{$sample: {size: num}}]).toArray();
    var listStatements = [];
    for(let i=0;i<listQuestions.length;i++){
        let entry = listQuestions[i];
        switch(entry.type){
            case "MC":
                let noOptionsTaken = (entry.options.length > 1) ? Math.floor((Math.random() * (entry.options.length-1)) + 2) : 1; // If there are more than 1 options, take 2 to N options to be played (can be not all). Otherwise, take 1.
                var optionsTaken = [];
                for(let k=0;k<noOptionsTaken;k++){
                        // Get randomized number of options
                        let optionTaken = null;
                        do{
                            optionTaken = entry.options[Math.floor((Math.random() * entry.options.length))];
                        }while(optionsTaken.includes(optionTaken));
                        optionsTaken.push(optionTaken);
                }
                for(let j=0;j<optionsTaken.length;j++){
                    // Compile statement per options taken
                    let statement = {question: entry.question, type: entry.type, option: optionsTaken[j].answer, expectedAnswer: optionsTaken[j].isCorrect};
                    listStatements.push(statement);
                }
                break;
            case "TF":
                statement = {question: entry.question, type: entry.type, expectedAnswer: entry.answer};
                listStatements.push(statement);
                break;
        }
    }
    return listStatements;
}
exports.compileQuestions = compileQuestions;

/**
 * Get list of questions for a game.
 * @param {*} db Database object
 * @param {*} gameId Game ID (as in the database)
 */
const getQuestions = async (db,gameId) => {
    var searchQuery = {_id: ObjectId(gameId)};
    var game = await db.collection('games').findOne(searchQuery);
    if(!game){
        console.error("Error in finding game.");
    }else{
        return game.questionSet.map(q => {
            delete q['expectedAnswer'];
            return q;
        });
    }
}
exports.getQuestions = getQuestions;

/**
 * Saves answers of questions played in a game to database.
 * @param {*} db        Database object
 * @param {*} gameId    Game ID (as in the database)
 * @param {*} answers   Array of answers
 */
const submitAnswer = async (db,gameId,answers) => {
    var game;
    try{
        let searchQuery = {_id: ObjectId(gameId)};
        let addAnswer = {$set:{resultSet: answers}};
        game = await db.collection('games').findOneAndUpdate(searchQuery,addAnswer,{returnOriginal:false});
        return game;
    } catch (error) {
        console.error(error);
    }
}
exports.submitAnswer = submitAnswer;

/**
 * Obtains the game result. Game result is a combination of question and answer arrays.
 * @param {*} db        Database object
 * @param {*} gameId    Game ID (as in the database)
 */
const getResult = async (db,gameId) => {
    var searchQuery = {_id: ObjectId(gameId)};
    var game;
    try{
        game = await db.collection('games').findOne(searchQuery);
    } catch (error) {
        console.error("Error in finding game.");
    }
    if(game && game.questionSet && game.resultSet){
        var results = game.resultSet.map((result) => {
            var expectedAnswer = game.questionSet.filter((question) => {
                return question.question === result.question && question.type === result.type && question.option === result.option;
            }).map((q) => {
                return q['expectedAnswer'];
            });
            result['expectedAnswer'] = expectedAnswer[0];
            return result;
        });
        return results;
    }
    else return null;
}
exports.getResult = getResult;