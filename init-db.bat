FOR /f "tokens=1,2 delims==" %%a IN (conf.settings) DO IF %%a==mongodb set "%%a=%%b"
FOR /f "tokens=1,2 delims==" %%a IN (conf.settings) DO IF %%a==mongodata set "%%a=%%b"
start %mongodb%\bin\mongod.exe --dbpath="%mongodata%"
timeout 5
%mongodb%\bin\mongo.exe --eval "db = db.getSiblingDB('werewolf-randomizer'); db.settings.insert({'key':'chars','value':['Werewolf','Seer','Doctor']});"
taskkill /IM mongod.exe /t