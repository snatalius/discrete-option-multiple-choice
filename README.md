# Discrete Option Multiple Choice (DOMC) simulator
_by Samuel Natalius_

This web application is created using Node.js, Express and MongoDB.
Additional dependency used: ejs, body-parser, nodemon

# How to run

## Windows

1. Make sure that Node.js is installed. Try to execute `npm` and `node` and check if it shows something.
2. Make sure that Nodemon is installed. If not, install it by running `npm install -g nodemon`.
Make sure that MongoDB is installed. Locate the path for `mongod.exe` and `mongo.exe`.
2. Create a folder to store MongoDB database file. Locate the path.
3. Open `conf.settings` file. Set `mongodb` with the directory path of MongoDB and `mongodata` with the folder for database file. For example: `mongodb=C:\mongodb\mongodb-win32-x86_64-2012plus-4.2.5`
4. Execute `init-db.bat` to initiate the database. (Do this only for the first time installation)
5. Execute `start.bat` to run the server.

## Linux

WIP: Similar concept to Windows (set up database then run the server).

## Docker

Simply run `docker compose up`.

Credits to: https://medium.com/zenofai/how-to-build-a-node-js-and-mongodb-application-with-docker-containers-15e535baabf5

# How to access MongoDB

The best way is to download and use MongoDB Compass. The DB can be accessed from host `localhost:27017`.

# Future Plan
- Enable SSL on the app's Docker container.
- Launch docker on Azure, check https://docs.docker.com/cloud/aci-integration/