var bodyParser = require('body-parser');
var express = require('express');
const MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var path = require('path');
var http = require('http');
var https = require('https');
var fs = require('fs');

var funcLib = require('./functionLibrary');
const { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } = require('constants');

// MONGODB

const dbUrl = 'mongodb://127.0.0.1:27017';
const dbName = 'discrete-option-multiple-choice';
let db;

MongoClient.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, (err, client) => {
  if (err) return console.log(err)

  // Storing a reference to the database so you can use it later
  db = client.db(dbName)
  console.log(`Connected MongoDB: ${dbUrl}`)
  console.log(`Database: ${dbName}`)
});

// EXPRESS

const hostname = '0.0.0.0';
const publicPath = __dirname + '/public/';

// TLS configuration (Using Self-signed certificate and key generated using OpenSSL)
var privateKey = fs.readFileSync('cert/priv.key');
var certificate = fs.readFileSync('cert/server.crt');

var credentials = {key: privateKey, cert: certificate};

// Use Express to serve files from the public directory
const app = express();
app.use(express.static(publicPath));
app.use("/css",express.static(publicPath + "css"));
app.use("/js",express.static(publicPath + "js"));

// Set EJS as view engine for Express render
app.set("view engine", "ejs"); 
app.set("views", publicPath);

// Set BodyParser to handle POST data
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Redirect HTTP to HTTPS
/*
app.use((req, res, next) => {
    req.secure ? next() : res.redirect("https://" + req.headers.host + req.url);
});
*/

// start the express web server listening on HTTP and HTTPS port
http.createServer(app).listen(80,hostname,() => {
    console.log("Listening to "+hostname+":"+80+" (HTTP)");
});
https.createServer(credentials,app).listen(443,hostname,() => {
    console.log("Listening to "+hostname+":"+443+" (HTTPS)");
});

// === ROUTE ===

// Homepage
app.get('/', async (req, res) => {
    var tnq = await funcLib.getTotalNumQuestion(db);
    res.render('main-page',{totalNumQuestion: tnq});
});

// Add Question
app.get('/addQuestion', async (req, res) => {
    res.render('add-question');
});

// Game room
app.get('/game/:gameId', async (req, res) => {
    console.debug(`gameId: ${req.params.gameId}`);
    res.render('game-room',{gameId: req.params.gameId});
});

// Result room
app.get('/result/:gameId', async (req, res) => {
    console.debug(`gameId: ${req.params.gameId}`);
    res.render('result-room',{gameId: req.params.gameId});
});

// === CONTROL ===

// Control: Start Game
app.post("/c/startGame", async (req,res) => {
    var playerName = req.body.inpName;
    var numQuestions = parseInt(req.body.inpNumQuestions);
    var game = await funcLib.createNewGame(db,playerName,numQuestions);
    res.redirect(`/game/${game._id}`);
});

// Control: Add Question
app.post("/c/addQuestion", async (req,res) => {
    var entry = req.body;
    entry.createdOn = new Date();
    console.debug(entry);
    await db.collection('questions').insertOne(entry);
    res.redirect(`/`);
});

app.get("/c/getQuestions/:gameId", async (req,res) => {
    var questions = await funcLib.getQuestions(db,req.params.gameId);
    console.log(questions);
    res.send(questions);
});

app.post("/c/submitAnswer/:gameId", async (req,res) => {
    var gameId = req.params.gameId;
    var answers = req.body.answers;
    var response = await funcLib.submitAnswer(db,gameId,answers);
    res.send("Answer submitted");
});

app.get("/c/getResult/:gameId",async (req,res) => {
    var gameId = req.params.gameId;
    res.send(await funcLib.getResult(db,gameId));
});

// === TEST ===

app.get("/test/compileQuestions", async (req,res) => {
    res.send(await funcLib.compileQuestions(db,4));
});