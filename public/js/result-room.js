var result;

const getResult = async (gameId) => {
  let result;

  try{
    result = await $.ajax({
      url: `/c/getResult/${gameId}`,
      type: 'GET',
      data: null
    });
  } catch (error) {
    alert('Error: text status '+textStatus+', err '+err);
  }

  return result;
}

const renderResult = (result) => {
  for (const r of result) {
    console.log(r);
    let html = `<tr> \
                <td>${r.question}</td> \
                <td>${r.option ? r.option : ""}</td> \
                <td>${r.expectedAnswer}</td> \
                <td ${r.userAnswer !== r.expectedAnswer ? 'style="background-color:salmon;"' : ''}>${r.userAnswer}</td> \
                </tr>`
    $("#details").find("tbody").append(html);
  }
}

const renderStats = (result) => {
  let htmlTS = `<h3>Total Statements played: ${result.length}</h3>`;
  let htmlCS = `<h3>Correct Answers: ${result.filter((r) => r.userAnswer === r.expectedAnswer).length}</h3>`;
  let htmlICS = `<h3>Incorrect Answers: ${result.filter((r) => r.userAnswer !== r.expectedAnswer).length}</h3>`;
  let score = result.filter((r) => r.userAnswer === r.expectedAnswer).length / result.length * 100;
  let htmlScore = `<h3>Score: ${score.toFixed(2)}%`;
  $("#details").append(htmlTS);
  $("#details").append(htmlCS);
  $("#details").append(htmlICS);
  $("#details").append(htmlScore);
}

$(document).ready(() => {
  const initResult = async () => {
    result = await getResult(gameId);
    renderResult(result);
    renderStats(result);
  }
  initResult();
  
  console.log(gameId + " ready!");
});