var optNum = 0;
const addOption = () => {
  optNum++;
  var html = `<tr id="option-${optNum}"> \
              <td><input type="text" id="inpOption[${optNum}][option]" placeholder="Answer ${optNum}"></td> \
              <td><input type="checkbox" id="inpOption[${optNum}][isCorrect]"><label for="inpOption[${optNum}][isCorrect]"></label></td> \
              <td><button class="button special" id="btnDeleteOption[${optNum}]" onClick="removeOption('#option-${optNum}');">X</button></td> \
              </tr>`;
  $('#options').append(html);
}
const removeOption = (elementId) => {
  if($('#options').children('tr').length > 1){
    $(elementId).remove(); // Removes an option from the document
  }
}

const compileOptions = () => {
  var options = [];
  $('#options').children('tr').each((index, element) => {
    options.push({'answer':$(element).find('input[type="text"]').val(),'isCorrect':$(element).find('input[type="checkbox"]').prop('checked')});
  });
  return options;
}

const compileQuestionEntry = () => {
  var entry = {};
  entry.question = $('#inpQuestion').val();
  let qType = $("input[name=inpQType]:checked").val();
  entry.type = qType;
  switch(qType){
    case "MC":
      entry.options = compileOptions();
      break;
    case "TF":
      entry.answer = $("input[name=inpAnswer]:checked").val();
      break;
  }
  return entry;
}

const submitQuestion = () => {
  $.post('/c/addQuestion',compileQuestionEntry(),() => {
    alert("Added Successfully");
    location.reload(true);
  });
}

$(document).ready(function(){

  const toogleQType = (type) => {
    switch(type){
      case "MC":
        console.debug("TF hide, MC show");
        $("#ansAreaTF").hide();
        $("#ansAreaMC").show();
        break;
      case "TF":
        console.debug("MC hide, TF show");
        $("#ansAreaMC").hide();
        $("#ansAreaTF").show();
        break;
    }
  }

  $("input[name=inpQType][value=MC]").prop("checked", true);
  toogleQType($("input[name=inpQType]:checked").val());

  $("input[name=inpQType]").change(() => {
    toogleQType($("input[name=inpQType]:checked").val());
  });
  
  addOption();
});