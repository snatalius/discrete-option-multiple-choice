var questions;
var answers = [];
var currentQNo;

const getQuestions = async () => {
  let result;
  
  try{
    result = await $.ajax({
      url: `/c/getQuestions/${gameId}`,
      type: 'GET',
      data: null
    });
  } catch (error) {
    alert('Error: text status '+textStatus+', err '+err);
  }
  return result;
}

const playQuestion = (qNo) => {
  if(questions && qNo < questions.length){
    let question = questions[qNo];
    $('#txtQuestion').text(question['question']);
    if(question['type'] === 'TF'){
      $('#txtOption').hide();
    } else {
      $('#txtOption').show();
      $('#txtOption').text(question['option']);
    }
  }
}

const doAnswer = (ans) => {
    saveAnswer(currentQNo,ans);
    currentQNo++;
    if(questions && currentQNo < questions.length){
      playQuestion(currentQNo);
    } else {
      finishGame();
    }
}

const saveAnswer = (qNo,ans) => {
  let answer = questions[qNo];
  answer['userAnswer'] = ans;
  answers.push(answer);
}

const finishGame = async () => {
  console.log("Game Finished");
  $('#txtQuestion').text("Game Finished");
  $('#txtOption').hide();

  let response;
  try{
    response = await $.ajax({
      url: `/c/submitAnswer/${gameId}`,
      type: 'POST',
      data: {answers: answers}
    });
  } catch (error) {
    alert('Error: text status '+textStatus+', err '+err);
  }
  
  // Redirect to result page
  window.location.href=`/result/${gameId}`;
}

$(document).ready(() => {
  const initGame = async () => {
    questions = await getQuestions();
    currentQNo = 0;
    playQuestion(currentQNo);
  }
  initGame();
  console.log(gameId + " ready!");
});